let day = new Date();
let hours = day.getHours();


let body = document.querySelector('body');
if (hours >= 18) {
    body.classList.add('night-bg');
    body.classList.remove('morning-bg');
} else {
    body.classList.add('morning-bg');
    body.classList.remove('night-bg');
}


let searchBtn = document.getElementById('searchBtn');

searchBtn.addEventListener('click', () => {
    let cityName = document.getElementById('city').value;

    let apiKey = 'ed9028c2a7ace11229e00308f7698ae9';
    let uri = 'https://api.openweathermap.org/data/2.5/forecast?q=' + cityName + '&appid=' + apiKey + `&lang=it` + `&units=metric`;

    let today = new Date();

    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;

    var req = new XMLHttpRequest();

    req.open('GET', uri, true);

    req.onload = function () {
       var data = JSON.parse(this.response);
       var temp = parseInt(data.list[0].main.temp);
       var maxTemp = parseInt(data.list[0].main.temp_max);
       var minTemp = parseInt(data.list[0].main.temp_min);
       var hum = data.list[0].main.humidity;
       var windSpeed = data.list[0].wind.speed;
       var windDegree = data.list[0].wind.deg;
       var weather = data.list[0].weather[0].description;
       var icon = data.list[0].weather[0].icon;
       var iconUrl = `http://openweathermap.org/img/wn/` + icon + `@2x.png`;
       

       resultWrapper = document.getElementById('resultWrapper').innerHTML = `
        <div id="tableResult" class="container borders">
            <div class="row text-center align-items-center tc-main line">
                <div class="col-12 col-md-3">
                    <p><img class="stroke" src="${iconUrl}" alt="Icona meteo"></p>
                </div>
                <div class="col-12 col-md-3">
                    <p><i class="fas fa-thermometer-half"></i> Temperatura:<text class="fs-2 fw-bold tc-accent stroke-black"> ${temp}°</text></p>
                    <p><i class="fas fa-thermometer-quarter"></i> Minime:<text class="tc-accent"> ${minTemp}°</text></p>
                    <p><i class="fas fa-thermometer-three-quarters"></i> Massime:<text class="tc-accent"> ${maxTemp}°</text></p>
                </div>
                <div class="col-12 col-md-3">
                    <p><i class="fas fa-tint"></i> Umidità:<text class="tc-accent"> ${hum}%</text></p>
                    <p><i class="fas fa-wind"></i> Vento:<text class="tc-accent"> ${windSpeed} km/h</text></p>
                </div>
                <div class="col-12 col-md-3">
                    <p class="fs-2"><i class="far fa-building fs-5"></i> <text class="fs-2 fw-bold tc-accent stroke-black"> ${cityName}</text></p>
                    <p><i class="far fa-calendar-alt"></i>  <text class="tc-accent">${today}</text></p>
                    <p><text class="tc-accent">${weather}</text></p>
                </div>
            </div>
        </div>
       `;

       let array = data.list;
       let fiveDays = [];

        array.forEach(element => {
            let newDay = new Date(element.dt * 1000).toLocaleDateString('it-IT');
            element.dt = newDay;
            let today = new Date().toLocaleDateString('it-IT');

            if(today !== element.dt && element.dt_txt.includes('09:00:00')) {
                fiveDays.push(element);
            }

        });

        console.log(array);
        console.log(fiveDays);

        let nextDays = document.getElementById('nextDays');
        nextDays.innerHTML = ` `;

            fiveDays.forEach(element => {
                var miniIcon = element.weather[0].icon;
                var miniIconUrl = `http://openweathermap.org/img/wn/` + miniIcon + `@2x.png`;

                let card = document.createElement('div');
                card.classList.add('col-12', 'col-md-2');
                
                card.innerHTML = `
                    <div class="card m-2 text-center shadow bg-meteo">
                        <div class="card-body">
                            <p class="card-title">${element.dt}</p>
                            <p><img class="stroke" src="${miniIconUrl}" alt=""></p>
                            <h5 class="card-text tc-accent">${element.main.temp}°</h5>
                        </div>
                    </div>
                `
            nextDays.appendChild(card);

            })

    }
    req.send();
    
    
})
